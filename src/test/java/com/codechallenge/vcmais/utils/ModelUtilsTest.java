package com.codechallenge.vcmais.utils;

import com.codechallenge.vcmais.model.Line;
import com.codechallenge.vcmais.model.Station;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ModelUtilsTest {

    @Test
    public void parseStationTest() {
        String csvAttribute = "1,51.5028,-0.2801,\"Acton Town\",\"Acton<br />Town\",3,2,0";
        Station result = ModelUtils.parseStation(csvAttribute);
        assertEquals(1, result.getId());
        assertEquals("51.5028", result.getLatitude());
        assertEquals("-0.2801", result.getLongitude());
        assertEquals("Acton Town", result.getName());
        assertEquals("Acton<br />Town", result.getDisplayName());
        assertEquals("3", result.getZone());
        assertEquals(2, result.getTotalLines());
    }

    @Test
    public void parseStationNullValueTest() {
        String csvAttribute = "2,51.5143,-0.0755,\"Aldgate\",NULL,1,2,0";
        Station result = ModelUtils.parseStation(csvAttribute);
        assertEquals("", result.getDisplayName());
    }

    @Test
    public void parseLineTest() {
        String csvAttribute = "1,\"Bakerloo Line\",\"ab6612\",NULL";
        Line result = ModelUtils.parseLine(csvAttribute);
        assertEquals(1, result.getId());
        assertEquals("Bakerloo Line", result.getName());
        assertEquals("ab6612", result.getColour());
        assertEquals("", result.getStripe());
    }

}
