package com.codechallenge.vcmais.business;

import com.codechallenge.vcmais.business.strategy.StepStrategy;
import com.codechallenge.vcmais.business.strategy.TimeStrategy;
import com.codechallenge.vcmais.model.Graph;
import com.codechallenge.vcmais.model.Line;
import com.codechallenge.vcmais.model.Station;
import com.codechallenge.vcmais.utils.ModelUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class UndergroundMapBusinessTest {

    @InjectMocks
    private UndergroundMapBusiness business;

    @Mock
    private Graph graph;

    @Mock
    private Map<Integer, Line> lines;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(business, "graph", graph);
        ReflectionTestUtils.setField(business, "lines", lines);
    }

    @Test
    public void loadStationsTest() {
        ArgumentCaptor<Station> captor = ArgumentCaptor.forClass(Station.class);
        List<String> entries = Arrays.asList("1,51.5028,-0.2801,\"Acton Town\",\"Acton<br />Town\",3,2,0", "2,51.5143,-0.0755,\"Aldgate\",NULL,1,2,0");

        business.loadStations(entries);

        verify(graph, times(entries.size())).addNode(captor.capture());
        assertEquals(1, captor.getAllValues().get(0).getId());
        assertEquals("Acton Town", captor.getAllValues().get(0).getName());
        assertEquals(2, captor.getAllValues().get(1).getId());
        assertEquals("Aldgate", captor.getAllValues().get(1).getName());
    }

    @Test
    public void loadLinesTest() {
        ArgumentCaptor<Line> captor = ArgumentCaptor.forClass(Line.class);
        List<String> entries = Arrays.asList("1,\"Bakerloo Line\",\"ab6612\",NULL", "3,\"Circle Line\",\"f7dc00\",NULL");

        business.loadLines(entries);

        verify(lines, times(entries.size())).put(anyInt(), captor.capture());
        assertEquals(1, captor.getAllValues().get(0).getId());
        assertEquals("Bakerloo Line", captor.getAllValues().get(0).getName());
        assertEquals(3, captor.getAllValues().get(1).getId());
        assertEquals("Circle Line", captor.getAllValues().get(1).getName());
    }

    @Test
    public void calculateShortestPathBetweenNodesSameLineTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesSameLine();

        List<Station> result = business.calculateShortestPathBetweenNodes(2, 5, new TimeStrategy());
        assertEquals(3, result.size());
        assertEquals(2, result.get(0).getId());
        assertEquals(4, result.get(1).getId());
        assertEquals(5, result.get(2).getId());
    }

    @Test
    public void calculateShortestPathDistanceBetweenNodesSameLineTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesSameLine();

        Integer result = business.calculateShortestPathDistanceBetweenNodes(2, 5, new TimeStrategy());
        assertEquals(6, result.intValue());
    }

    @Test
    public void calculateShortestPathDifferentLinesTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesDifferentLines();

        List<Station> result = business.calculateShortestPathBetweenNodes(3, 5, new TimeStrategy());
        assertEquals(5, result.size());
        assertEquals(3, result.get(0).getId());
        assertEquals(1, result.get(1).getId());
        assertEquals(2, result.get(2).getId());
        assertEquals(4, result.get(3).getId());
        assertEquals(5, result.get(4).getId());
    }

    @Test
    public void calculateShortestPathDistanceDifferentLinesTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesDifferentLines();

        Integer result = business.calculateShortestPathDistanceBetweenNodes(3, 5, new TimeStrategy());
        assertEquals(21, result.intValue());
    }

    @Test
    public void calculateShortestPathCrossLinesTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesCrossLines();

        List<Station> result = business.calculateShortestPathBetweenNodes(3, 5, new TimeStrategy());
        assertEquals(6, result.size());
        assertEquals(3, result.get(0).getId());
        assertEquals(1, result.get(1).getId());
        assertEquals(2, result.get(2).getId());
        assertEquals(6, result.get(3).getId());
        assertEquals(7, result.get(4).getId());
        assertEquals(5, result.get(5).getId());
    }

    @Test
    public void calculateShortestPathDistanceCrossLinesTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesCrossLines();

        Integer result = business.calculateShortestPathDistanceBetweenNodes(3, 5, new TimeStrategy());
        assertEquals(15, result.intValue());
    }

    @Test
    public void calculateShortestPathBetweenNodesSameLineByStepTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesSameLine();

        List<Station> result = business.calculateShortestPathBetweenNodes(1, 4, new StepStrategy());
        assertEquals(3, result.size());
        assertEquals(1, result.get(0).getId());
        assertEquals(2, result.get(1).getId());
        assertEquals(4, result.get(2).getId());
    }

    @Test
    public void calculateShortestPathDistanceBetweenNodesSameLineByStepTest() {
        Graph customGraph = createGraph();
        ReflectionTestUtils.setField(business, "graph", customGraph);
        loadRoutesSameLine();

        Integer result = business.calculateShortestPathDistanceBetweenNodes(1, 7, new StepStrategy());
        assertEquals(3, result.intValue());
    }

    private Graph createGraph() {
        String s1 = "1,51.5028,-0.2801,\"Acton Town\",\"Acton<br />Town\",3,2,0";
        String s2 = "2,51.5143,-0.0755,\"Aldgate\",NULL,1,2,0";
        String s3 = "3,51.5154,-0.0726,\"Aldgate East\",\"Aldgate<br />East\",1,2,0";
        String s4 = "4,51.5107,-0.013,\"All Saints\",\"All<br />Saints\",2,1,0";
        String s5 = "5,51.5407,-0.2997,\"Alperton\",NULL,4,1,0";
        String s6 = "6,51.6736,-0.607,\"Amersham\",NULL,10,1,1";
        String s7 = "7,51.5322,-0.1058,\"Angel\",NULL,1,1,0";
        List<String> inputList = Arrays.asList(s1, s2, s3, s4, s5, s6, s7);
        Graph customgraph = new Graph();
        inputList.stream().map(ModelUtils::parseStation).forEach(customgraph::addNode);
        return customgraph;
    }

    private void loadRoutesSameLine() {
        String r1 = "1,2,1";
        String r2 = "1,3,1";
        String r3 = "2,4,1";
        String r4 = "2,6,1";
        String r5 = "3,5,1";
        String r6 = "4,5,1";
        String r7 = "4,6,1";
        String r8 = "6,7,1";
        String r9 = "7,5,1";
        business.loadRoutes(Arrays.asList(r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    private void loadRoutesDifferentLines() {
        String r1 = "1,2,1";
        String r2 = "1,3,1";
        String r3 = "2,4,2";
        String r4 = "2,6,1";
        String r5 = "4,5,2";
        business.loadRoutes(Arrays.asList(r1, r2, r3, r4, r5));
    }

    private void loadRoutesCrossLines() {
        String r1 = "1,2,1";
        String r2 = "1,3,1";
        String r3 = "2,4,2";
        String r4 = "2,6,1";
        String r5 = "4,5,2";
        String r6 = "4,6,1";
        String r7 = "6,7,1";
        String r8 = "7,5,1";
        business.loadRoutes(Arrays.asList(r1, r2, r3, r4, r5, r6, r7, r8));
    }
}
