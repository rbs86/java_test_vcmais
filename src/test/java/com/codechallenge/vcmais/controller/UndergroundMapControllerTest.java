package com.codechallenge.vcmais.controller;

import com.codechallenge.vcmais.VcmaisApplication;
import com.codechallenge.vcmais.business.UndergroundMapBusiness;
import com.codechallenge.vcmais.business.strategy.RandomStrategy;
import com.codechallenge.vcmais.business.strategy.StepStrategy;
import com.codechallenge.vcmais.business.strategy.Strategy;
import com.codechallenge.vcmais.business.strategy.TimeStrategy;
import com.codechallenge.vcmais.model.Station;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VcmaisApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, properties = {"server.port=10100"})
public class UndergroundMapControllerTest {

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private UndergroundMapController controller;

    @Mock
    private UndergroundMapBusiness business;

    private String baseUrl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(controller, "business", business);
        baseUrl = "http://localhost:10100/";
    }

    @Test
    public void shortestPathTest() {
        callController("time", TimeStrategy.class);
    }

    @Test
    public void shortestPathRandomStrategyTest() {
        callController("random", RandomStrategy.class);
    }

    @Test
    public void shortestPathStepStrategyTest() {
        callController("step", StepStrategy.class);
    }

    @Test
    public void shortestPathDistanceTest() {
        String url = baseUrl + "shortestpath/source/10/destination/200/time/distance";
        ResponseEntity<Integer> response = restTemplate.exchange(url, HttpMethod.GET, null, Integer.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(business).calculateShortestPathDistanceBetweenNodes(eq(10), eq(200), any(TimeStrategy.class));
    }

    private void callController(String strategy, Class<? extends Strategy> stClass) {
        String url = baseUrl + "shortestpath/source/10/destination/200/" + strategy;
        ResponseEntity<List<Station>> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Station>>() {
        });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(business).calculateShortestPathBetweenNodes(eq(10), eq(200), any(stClass));
    }

}
