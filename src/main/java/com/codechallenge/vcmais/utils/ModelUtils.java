package com.codechallenge.vcmais.utils;

import com.codechallenge.vcmais.model.Line;
import com.codechallenge.vcmais.model.Station;

public class ModelUtils {

    private static final String NULL_INPUT = "NULL";

    private ModelUtils() {
        //private constructor to hide the implicit public one
    }

    public static Station parseStation(String csvAttrs) {
        String[] attrs = csvAttrs.replace("\"", "").replace(NULL_INPUT, "").split(",");
        return Station.builder().id(Integer.parseInt(attrs[0]))
                .latitude(attrs[1])
                .longitude(attrs[2])
                .name(attrs[3])
                .displayName(attrs[4])
                .zone(attrs[5])
                .totalLines(Integer.parseInt(attrs[6]))
                .build();
    }

    public static Line parseLine(String csvAttrs) {
        String[] attrs = csvAttrs.replace("\"", "").split(",");
        return Line.builder()
                .id(Integer.parseInt(attrs[0]))
                .name(attrs[1])
                .colour(attrs[2])
                .stripe(defaultIfEquals(attrs[3], NULL_INPUT, ""))
                .build();
    }

    private static String defaultIfEquals(String strToValidate, String comparison, String defaultStr) {
        return strToValidate.equals(comparison) ? defaultStr : strToValidate;
    }
}
