package com.codechallenge.vcmais.business;

import com.codechallenge.vcmais.business.strategy.Strategy;
import com.codechallenge.vcmais.model.Graph;
import com.codechallenge.vcmais.model.Line;
import com.codechallenge.vcmais.model.Station;
import com.codechallenge.vcmais.utils.ModelUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class UndergroundMapBusiness {

    private static Graph graph = new Graph();
    private static Map<Integer, Line> lines = new HashMap<>();

    void loadStations(List<String> entries) {
        entries.stream().map(ModelUtils::parseStation).forEach(graph::addNode);
    }

    void loadLines(List<String> entries) {
        entries.stream().map(ModelUtils::parseLine).forEach(l -> lines.put(l.getId(), l));
    }

    void loadRoutes(List<String> entries) {
        entries.forEach(this::saveRoutesData);
    }

    /**
     * Algoritmo de Dijkstra adaptado
     *
     * @param sourceId      Source station id
     * @param destinationId Destination station id
     * @return Station list
     */
    public List<Station> calculateShortestPathBetweenNodes(int sourceId, int destinationId, Strategy strategy) {
        Station source = graph.getStations().get(sourceId);
        Station destination = graph.getStations().get(destinationId);
        calculateShortestPathFromSource(source, strategy);
        Map<Integer, Station> nodes = graph.getStations();
        Station stt = nodes.get(destination.getId());

        List<Station> path = new ArrayList<>(stt.getShortestPath());
        path.add(stt);
        return path;
    }

    public Integer calculateShortestPathDistanceBetweenNodes(int sourceId, int destinationId, Strategy strategy) {
        Station source = graph.getStations().get(sourceId);
        Station destination = graph.getStations().get(destinationId);
        calculateShortestPathFromSource(source, strategy);
        Map<Integer, Station> nodes = graph.getStations();
        return nodes.get(destination.getId()).getDistance();
    }

    private void calculateShortestPathFromSource(Station source, Strategy strategy) {
        source.setDistance(0);
        source.setCurrentLines(source.getLines());

        Set<Station> settleNodes = new HashSet<>();
        Set<Station> unsettleNodes = new HashSet<>();
        unsettleNodes.add(source);

        while (!unsettleNodes.isEmpty()) {
            Station currentNode = strategy.nextMove(unsettleNodes);
            unsettleNodes.remove(currentNode);
            for (Map.Entry<Station, Integer> adjacencyPair : currentNode.getAdjacentStations().entrySet()) {
                Station adjacentNode = adjacencyPair.getKey();
                Integer line = adjacencyPair.getValue();

                if (!settleNodes.contains(adjacentNode)) {
                    calculateMinimuDistance(adjacentNode, line, currentNode, strategy);
                    unsettleNodes.add(adjacentNode);
                }
            }
            settleNodes.add(currentNode);
        }
    }

    private void calculateMinimuDistance(Station evaluationNode, Integer line, Station sourceNode, Strategy strategy) {
        Integer sourceDistance = sourceNode.getDistance();
        if (sourceDistance + strategy.getDefaultWeight() < evaluationNode.getDistance()) {
            int distanceSum = strategy.getDistance(sourceNode, line);
            evaluationNode.setDistance(distanceSum);
            LinkedList<Station> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
            evaluationNode.setCurrentLines(updateCurrentLine(sourceNode.getCurrentLines(), new HashSet<>(Arrays.asList(line))));
        }
    }

    private Set<Integer> updateCurrentLine(Set<Integer> currentLines, Set<Integer> currentNodeLines) {
        Set<Integer> newLines = currentLines.stream().filter(currentNodeLines::contains).collect(Collectors.toSet());
        return !CollectionUtils.isEmpty(newLines) ? newLines : currentNodeLines;
    }

    private void saveRoutesData(String entry) {
        String[] route = entry.split(",");
        Integer from = Integer.parseInt(route[0]);
        Integer to = Integer.parseInt(route[1]);
        int line = Integer.parseInt(route[2]);

        Station stationSource = graph.getStations().get(from);
        stationSource.addLine(line);
        Station stationDestination = graph.getStations().get(to);
        stationDestination.addLine(line);

        stationSource.addDestination(stationDestination, line);
        stationDestination.addDestination(stationSource, line);
    }
}
