package com.codechallenge.vcmais.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Component
public class LoadFileComponent {

    @Value("classpath:/files/stations.csv")
    private Resource stationsResource;

    @Value("classpath:/files/lines.csv")
    private Resource linesResource;

    @Value("classpath:/files/routes.csv")
    private Resource routesResource;

    @Autowired
    private UndergroundMapBusiness business;

    @PostConstruct
    public void loadFiles() throws IOException {
        List<String> entries = Files.readAllLines(Paths.get(stationsResource.getURI()));
        business.loadStations(entries);

        entries = Files.readAllLines(Paths.get(linesResource.getURI()));
        business.loadLines(entries);

        entries = Files.readAllLines(Paths.get(routesResource.getURI()));
        business.loadRoutes(entries);
    }
}
