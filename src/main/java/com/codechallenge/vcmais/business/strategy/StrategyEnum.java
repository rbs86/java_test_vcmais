package com.codechallenge.vcmais.business.strategy;

public enum StrategyEnum {

    STEP, TIME, RANDOM;
}
