package com.codechallenge.vcmais.business.strategy;

import com.codechallenge.vcmais.model.Station;

import java.util.Set;

public interface Strategy {

    Station nextMove(Set<Station> unsettleNodes);

    Integer getDistance(Station sourceNode, Integer line);

    Integer getDefaultWeight();

}
