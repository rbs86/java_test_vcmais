package com.codechallenge.vcmais.business.strategy;

import com.codechallenge.vcmais.model.Station;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TimeStrategy implements Strategy {

    private static final int DEFAULT_TIME = 3;
    private static final int CHANGE_LINE_EXTRA_TIME = 9;

    @Override
    public Station nextMove(Set<Station> unsettleNodes) {
        Station lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;

        for (Station node : unsettleNodes) {
            int nodeDistance = node.getDistance() + (isSameLine(node.getCurrentLines(), node.getLines()) ? 0 : CHANGE_LINE_EXTRA_TIME);
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    @Override
    public Integer getDistance(Station sourceNode, Integer line) {
        boolean isSameLine = isSameLine(sourceNode.getCurrentLines(), new HashSet<>(Arrays.asList(line)));
        return sourceNode.getDistance() + DEFAULT_TIME + (isSameLine ? 0 : CHANGE_LINE_EXTRA_TIME);
    }

    @Override
    public Integer getDefaultWeight() {
        return DEFAULT_TIME;
    }

    private boolean isSameLine(Set<Integer> currentLines, Set<Integer> currentNodeLines) {
        List<Integer> newLines = currentLines.stream().filter(currentNodeLines::contains).collect(Collectors.toList());
        return !CollectionUtils.isEmpty(newLines);
    }
}
