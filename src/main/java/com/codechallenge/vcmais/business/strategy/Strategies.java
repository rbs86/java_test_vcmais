package com.codechallenge.vcmais.business.strategy;

public class Strategies {

    private Strategies() {
        //private constructor to hide the implicit public one
    }

    public static Strategy of(StrategyEnum st) {
        switch (st) {
            case TIME:
                return new TimeStrategy();
            case RANDOM:
                return new RandomStrategy();
            case STEP:
                return new StepStrategy();
            default:
                throw new IllegalArgumentException("Illegal strategy parameter!");
        }
    }
}
