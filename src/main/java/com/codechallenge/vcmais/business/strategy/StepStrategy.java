package com.codechallenge.vcmais.business.strategy;

import com.codechallenge.vcmais.model.Station;

import java.util.Set;

public class StepStrategy implements Strategy {

    private static final int DEFAULT_WEIGHT = 1;

    @Override
    public Station nextMove(Set<Station> unsettleNodes) {
        Station lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;

        for (Station node : unsettleNodes) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    @Override
    public Integer getDistance(Station sourceNode, Integer line) {
        return sourceNode.getDistance() + DEFAULT_WEIGHT;
    }

    @Override
    public Integer getDefaultWeight() {
        return DEFAULT_WEIGHT;
    }
}
