package com.codechallenge.vcmais.business.strategy;

import com.codechallenge.vcmais.model.Station;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

public class RandomStrategy implements Strategy {

    private static final int DEFAULT_WEIGHT = 1;

    @Override
    public Station nextMove(Set<Station> unsettleNodes) {
        Random random = new Random();
        int val = random.nextInt(100) % unsettleNodes.size();
        return new ArrayList<>(unsettleNodes).get(val);
    }

    @Override
    public Integer getDistance(Station sourceNode, Integer line) {
        return sourceNode.getDistance() + DEFAULT_WEIGHT;
    }

    @Override
    public Integer getDefaultWeight() {
        return DEFAULT_WEIGHT;
    }
}
