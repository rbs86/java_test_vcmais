package com.codechallenge.vcmais.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Line {

    private int id;
    private String name;
    private String colour;
    private String stripe;
}
