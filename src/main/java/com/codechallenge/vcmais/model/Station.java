package com.codechallenge.vcmais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Builder
public class Station {

    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String displayName;

    @Getter
    @Setter
    private String zone;

    @Getter
    @Setter
    private int totalLines;

    @Getter
    @Setter
    private int rail;

    @Getter
    @Setter
    private String latitude;

    @Getter
    @Setter
    private String longitude;

    @Getter
    @Setter
    @Builder.Default
    @JsonIgnore
    private List<Station> shortestPath = new LinkedList<>();

    @Getter
    @Setter
    @Builder.Default
    @JsonIgnore
    private Integer distance = Integer.MAX_VALUE;

    @Getter
    @Setter
    @Builder.Default
    @JsonIgnore
    private Map<Station, Integer> adjacentStations = new HashMap<>();

    @Getter
    @Setter
    @Builder.Default
    @JsonIgnore
    private Set<Integer> lines = new HashSet<>();

    @Getter
    @Setter
    @JsonIgnore
    private Set<Integer> currentLines;

    @Tolerate
    public Station(Integer id) {
        this.id = id;
    }

    public void addDestination(Station destination, int line) {
        if (CollectionUtils.isEmpty(adjacentStations)) {
            adjacentStations = new HashMap<>();
        }
        adjacentStations.put(destination, line);
    }

    public void addLine(Integer lineId) {
        if (CollectionUtils.isEmpty(lines)) {
            lines = new HashSet<>();
        }
        lines.add(lineId);
    }

}
