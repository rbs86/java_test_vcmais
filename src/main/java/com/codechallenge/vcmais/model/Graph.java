package com.codechallenge.vcmais.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Graph {
    private Map<Integer, Station> stations = new HashMap<>();

    public void addNode(Station station) {
        this.stations.put(station.getId(), station);
    }
}
