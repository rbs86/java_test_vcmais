package com.codechallenge.vcmais.controller;

import com.codechallenge.vcmais.business.UndergroundMapBusiness;
import com.codechallenge.vcmais.business.strategy.Strategies;
import com.codechallenge.vcmais.business.strategy.Strategy;
import com.codechallenge.vcmais.business.strategy.StrategyEnum;
import com.codechallenge.vcmais.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class UndergroundMapController {

    @Autowired
    private UndergroundMapBusiness business;

    @GetMapping("shortestpath/source/{source}/destination/{destination}/{strategy}")
    public List<Station> shortestPath(@PathVariable(value = "source") Integer source,
                                      @PathVariable(value = "destination") Integer destination,
                                      @PathVariable(value = "strategy") String strategy) {
        Strategy st = Strategies.of(StrategyEnum.valueOf(strategy.toUpperCase()));
        return business.calculateShortestPathBetweenNodes(source, destination, st);
    }

    @GetMapping("shortestpath/source/{source}/destination/{destination}/{strategy}/distance")
    public Integer shortestPathDistance(@PathVariable(value = "source") Integer source,
                                        @PathVariable(value = "destination") Integer destination,
                                        @PathVariable(value = "strategy") String strategy) {
        Strategy st = Strategies.of(StrategyEnum.valueOf(strategy.toUpperCase()));
        return business.calculateShortestPathDistanceBetweenNodes(source, destination, st);
    }
}
