## Code Challenge VC+

Você foi contratado para construir um **webservice REST** para o metrô de Londres. Você recebeu os arquivos em **[1]** para alimentar o banco de dados desse serviço.

1. Você deve criar um método para importar os arquivos existentes e uma estrutura para armazenar os dados, a importação deve ser feita apenas uma vez.

Ao finalizar a primeira parte, as seguintes funcionalidades foram pedidas pelo time de mobile para que eles possam construir uma aplicação para auxiliar no deslocamento dos passageiros.

1. Um método que liste um caminho (contendo todas as estações) qualquer entre a estação X e a estação Y

2. Um método que liste o menor caminho (contendo todas as estações) (considerando a quantidade de paradas como requisito para o menor caminho) entre a estação X e a estação Y

3. Um método que calcule o tempo aproximando da viagem no item 2, considerando que ao passar de uma estação adjacente à próxima, o passageiro gaste 3 minutos e ao trocar de linha gaste 12 minutos.

**Observações:**

1. Tanto o desenho da arquitetura do serviço assim como os testes unitários fazem parte da resolução do teste.
2. O retorno do webservice REST deve ser JSON
3. O código deve ser hospedado em um repositório forkado a partir desse.
4. Ao terminar o teste mande um email para code-challenge@vcmais.com com o link para seu repositório.

**Recursos: [1]**

[London Underground Geographic Maps - CSV](https://commons.wikimedia.org/wiki/London_Underground_geographic_maps/CSV) (para facilitar os arquivos foram inseridos no respositório.)

## Explicação

### Aplicação

Aplicação feita com Java 8 e Spring Boot 2.

### Endpoints

[GET] shortestpath/source/{source}/destination/{destination}/{strategy}
> Retorna a lista de estações

[GET] shortestpath/source/{source}/destination/{destination}/{strategy}/distance
> Retorna o tempo ou distância para o caminho de acordo com a estratégia adotada 

|var            | tipo      | required  | description	|
|----		    |----		|----		|----		|
|source         | int       | true      | ID da estação de partida |
|destination    | int       | true      | ID da estação destino |
|strategy       | string    | true      | Define a estratégia para recuperar o resultado. Possíveis valores: step (considera a quantidade de paradas), time 
(considera o tempo gasto) e random (percorre as estações de forma aleatória) |

### Requests

#### Recuperando caminho qualquer entre as estações X e Y
```
curl localhost:8080/shortestpath/source/1/destination/110/random

[
  {
    "id": 1,
    "name": "Acton Town",
    "displayName": "Acton<br />Town",
    "zone": "3",
    "totalLines": 2,
    "rail": 0,
    "latitude": "51.5028",
    "longitude": "-0.2801"
  },
  {
    "id": 265,
    "name": "Turnham Green",
    "displayName": "Turnham<br />Green",
    "zone": "2.5",
    "totalLines": 2,
    "rail": 0,
    "latitude": "51.4951",
    "longitude": "-0.2547"
  },
  {
    "id": 110,
    "name": "Hammersmith",
    "displayName": "",
    "zone": "2",
    "totalLines": 3,
    "rail": 0,
    "latitude": "51.4936",
    "longitude": "-0.2251"
  }
]
```

#### Recuperando menor caminho entre as estações X e Y considerando número de paradas
```
curl localhost:8080/shortestpath/source/1/destination/110/step

[
  {
    "id": 1,
    "name": "Acton Town",
    "displayName": "Acton<br />Town",
    "zone": "3",
    "totalLines": 2,
    "rail": 0,
    "latitude": "51.5028",
    "longitude": "-0.2801"
  },
  {
    "id": 265,
    "name": "Turnham Green",
    "displayName": "Turnham<br />Green",
    "zone": "2.5",
    "totalLines": 2,
    "rail": 0,
    "latitude": "51.4951",
    "longitude": "-0.2547"
  },
  {
    "id": 110,
    "name": "Hammersmith",
    "displayName": "",
    "zone": "2",
    "totalLines": 3,
    "rail": 0,
    "latitude": "51.4936",
    "longitude": "-0.2251"
  }
]
```

#### Recuperando menor caminho entre as estações X e Y considerando tempo gasto
```
curl localhost:8080/shortestpath/source/1/destination/110/time

[
  {
    "id": 1,
    "name": "Acton Town",
    "displayName": "Acton<br />Town",
    "zone": "3",
    "totalLines": 2,
    "rail": 0,
    "latitude": "51.5028",
    "longitude": "-0.2801"
  },
  {
    "id": 265,
    "name": "Turnham Green",
    "displayName": "Turnham<br />Green",
    "zone": "2.5",
    "totalLines": 2,
    "rail": 0,
    "latitude": "51.4951",
    "longitude": "-0.2547"
  },
  {
    "id": 110,
    "name": "Hammersmith",
    "displayName": "",
    "zone": "2",
    "totalLines": 3,
    "rail": 0,
    "latitude": "51.4936",
    "longitude": "-0.2251"
  }
]
```

#### Recuperando tempo total para percorrer menor caminho entre as estações X e Y considerando tempo gasto
```
curl localhost:8080/shortestpath/source/1/destination/110/time/distance

6
```

#### Recuperando a quantidade de estações para percorrer o menor caminho entre as estações X e Y considerando tempo gasto
```
curl localhost:8080/shortestpath/source/1/destination/110/step/distance

2
```
